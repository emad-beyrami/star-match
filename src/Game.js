import React, { Component, useEffect, useState } from 'react';
import utils from './utils/utils';
import PlayNumber from './PlayNumber';
import StarsDisplay from './StartsDisplay';
import PlayAgain from './PlayAgain';


const Game = (props) => {
    const [stars, setStars] = useState(utils.random(1, 9));
    const [availableNumbers, setAvailableNumbers] = useState(utils.range(1, 9));
    const [candidateNums, setCandidateNums] = useState([]);
    const [secondsLeft, setSecondsLeft] = useState(10);

    //setTimeout
    useEffect(() => {
        if (secondsLeft > 0 && availableNumbers.length > 0) {
            const timerId = setTimeout(() => {
                setSecondsLeft(secondsLeft - 1);
            }, 1000)
            return () => clearTimeout(timerId);
        }
    });

    const candidatesAreWrong = utils.sum(candidateNums) > stars;
    // const gameIsWone = availableNumbers.length === 0;
    // const gameIsLost = secondsLeft === 0

    const gameStatus = availableNumbers.length === 0
        ? 'won'
        : secondsLeft === 0 ? 'lost' : 'active'

    // const resetGame = () => {
    //     setStars(utils.random(1, 0));
    //     setAvailableNumbers(utils.range(1, 9));
    //     setCandidateNums([]);
    // }

    const numberStatus = number => {
        if (!availableNumbers.includes(number)) {
            return 'used';
        }
        if (candidateNums.includes(number)) {
            return candidatesAreWrong ? 'wrong' : 'candidate';
        }
        return 'available';
    };

    const onNumberClick = (number, currentStatus) => {
        if (gameStatus !== 'active' || currentStatus === 'used') {
            return;
        }

        const newCandidateNums =
            currentStatus === 'available'
                ? candidateNums.concat(number)
                : candidateNums.filter(cn => cn !== number)

        if (utils.sum(newCandidateNums) !== stars) {
            setCandidateNums(newCandidateNums);
        } else {
            const newAvailableNums = availableNumbers.filter(
                n => !newCandidateNums.includes(n)
            );
            //redraw stars 
            setStars(utils.randomSumIn(newAvailableNums, 9))
            setAvailableNumbers(newAvailableNums);
            setCandidateNums([])
        }
    }


    return (
        <div className="game">
            <div className="help">
                Pick 1 or more numbers that sum to the number of stars
        </div>
            <div className="body">
                <div className="left">
                    {gameStatus !== 'active' ? (
                        <PlayAgain gameStatus={gameStatus} onClick={props.startNewGame} />
                    ) : (
                            <StarsDisplay count={stars} />
                        )}
                </div>
                <div className="right">
                    {utils.range(1, 9).map(number =>
                        <PlayNumber
                            key={number}
                            status={numberStatus(number)}
                            onClick={onNumberClick}
                            number={number} />
                    )}
                </div>
            </div>
            <div className="timer">Time Remaining: {secondsLeft}</div>
        </div>
    );
};



export default Game;
